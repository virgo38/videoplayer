/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;

/**
 *
 * @author virgo
 */
class Device implements Comparable<Device>{
    private File dir;
    private String tsGosNumber;
    private String deviceId;

    public Device(File dir, String tsGosNumber) {
        this.dir = dir;
        this.tsGosNumber = tsGosNumber;
        this.deviceId = dir.getName();
    }

    public File getDir() {
        return dir;
    }

    public void setDir(File dir) {
        this.dir = dir;
    }

    public String getTsGosNumber() {
        return tsGosNumber;
    }

    public void setTsGosNumber(String tsGosNumber) {
        this.tsGosNumber = tsGosNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceName) {
        this.deviceId = deviceName;
    }
    
    @Override
    public String toString() {
        return String.format("%s(%s)", tsGosNumber, deviceId);
    }
    
    public int compareTo(Device device) {
        int delta = Integer.parseInt(this.tsGosNumber) - Integer.parseInt(device.getTsGosNumber());
        if (delta == 0){
            return Integer.parseInt(this.getDeviceId()) - Integer.parseInt(device.getDeviceId());
        }
        return delta;
    }

}
