/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import com.googlecode.mp4parser.DirectFileReadDataSource;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.tracks.h264.H264TrackImpl;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import sql.SqliteDB;
import utils.Record;
import video.MediaControl;
/**
 *
 * @author virgo
 */
public class MainFrame extends javax.swing.JFrame {
    // Отладка.
    private boolean DEBUG = true;
    // Полноэкранный режим.
    private boolean FULL_SCREEN = false;
    // Без запуска видео.
    private boolean WITHOUT_VIDEO = false;
    
    private static String MP4_EXTENSION = ".mp4";
    private static String H264_EXTENSION = ".h264";
    private static String TEST_DIR_PATH = "test_dir";
    
    private String dirPath;
    private JList jListDevices;
    private JList jListFiles;
    private DefaultListModel listFilesModel = new DefaultListModel();
    private DefaultListModel listDevicesModel = new DefaultListModel();
    
    final JFXPanel fxPanel = new JFXPanel();
    private Media curMedia;
    private MediaPlayer mediaPlayer;
    private MediaControl mediaControl;
    
    // Кэш. Ключ: [директория:имя_файла]
    private HashMap cashData = new  HashMap<String, Record>();
    private Record curVideoData;
    private File prevVideoFile;
    private boolean forceChangeValue = false;
    private int MARGIN = 20;
    
    private SqliteDB db;
    private String tbManCount = "tb_man_count";
    private String tbTsUnitLink = "tb_ts_vunit_link";
    private String tbTransport = "tb_transport";
    
    private Device currentDevice;

    /**
     * Creates new form MainFrame
     * @param dbPath путь к БД
     * @param dirPath путь к директории с файлами
     */
    public MainFrame(String dbPath, String dirPath) {
        db = new SqliteDB(dbPath);
        
        if (this.DEBUG){
            System.out.println("DEGUG == TRUE");
            this.dirPath = dirPath == null ? this.TEST_DIR_PATH : dirPath;
            System.out.println(String.format("dirPath == %s", this.dirPath));
            System.out.println(String.format("dbPath == %s", db.getDbPath()));
        }
        
        initComponents();
        loadDirAndFilesList();
        initHotKeys();
        
        jPanelVideo.add(fxPanel);
        
        
        // Делаем размером во весь экран.
        // Определяем разрешение экрана монитора
        if (FULL_SCREEN){
            this.setExtendedState(this.MAXIMIZED_BOTH);
        }

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                initFX(fxPanel);
            }
        });
    }
    
    ///////////// Initial /////////////
    private void initFX(JFXPanel fxPanel){
        Group root = new Group();
        Scene scene = new Scene(root, 540, 241);
        fxPanel.setScene(scene);
    }
    
    private void loadDirAndFilesList() {
        File root = new File(this.dirPath);
        if (!root.exists()) {
            return;
        }

        // Только директории у которых не меньше одного файла в корне.
        FileFilter dirFilter;
        dirFilter = new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.isFile()) {
                    return false;
                }

                // Только файлы.
                File[] childrens = file.listFiles(geth264FileFilter());
                return (childrens != null) && (childrens.length >= 1);
            }
        };

        File[] dirs = root.listFiles(dirFilter);
        if (dirs == null || dirs.length == 0) {
            String msg = "Нет доступных директорий.";
            showMessage(msg, "Error");
            return;
        }
        
        // Список устройст. из директорий.
        for (File dir: dirs){
            // Проверка имени директории. Образец 510.
            Pattern p = Pattern.compile("^[0-9]{1,10}$");  
            Matcher m = p.matcher(dir.getName());
            if (!m.matches()){
                String msg = String.format("Неправильное название устройства: %s", dir.getName());
                this.showMessage(msg, "Error");
                continue;
            }
            
            // Проверяем есть соотв. ТС.
            String tsNumber = getTsNumber(dir.getName());
            if (tsNumber == null){
                String msg = String.format("Нет ТС для устройства %s", dir.getName());
                this.showMessage(msg, "Error");
                continue;
            }
            
            Device device = new Device(dir, tsNumber);
            listDevicesModel.addElement(device);
        }
        
        // Сортируем устройства.
        sortModel(listDevicesModel);
        
        jListDevices = new JList(listDevicesModel);
        jListDevices.setFocusable(false);

        // Список файлов.
        jListFiles = new JList(listFilesModel);
        jListFiles.setCellRenderer(new FileListCellRenderer());  
        JScrollPane jScrollPanel = new JScrollPane(jListFiles);
        
        jPanelFiles.add(jScrollPanel);
        jListFiles.setSize(jPanelFiles.getWidth(), 
                jPanelFiles.getHeight());
        jPanelFiles.revalidate();

        // Смена директории.
        jListDevices.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                setEnableControlButton(false);
                currentDevice =  (Device) jListDevices.getSelectedValue();

                updateListFiles();
                
                // Убираем текущий видео файл. Очищаем текстовые поля.
                curVideoData = null;
                prevVideoFile = null;
                clearInputOutputDataAndTextField();
                
                // Дисбаланс.
                boolean res = calculate();
                if (!res){
                    setEnableControlButton(false);
                    showMessage("Ошибка, при получении баланса.", "Error");
                }
            }
        });

        // Смена файла.
        jListFiles.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                File file = (File) jListFiles.getSelectedValue();
                if (file == null) {
                    return;
                }
                
                // Добавляем в кэш, если необходимо.
                String fName = file.getName();
                String key = String.format("%s:%s", currentDevice.getDeviceId(), fName);
                curVideoData = (Record)cashData.get(key);

                if (curVideoData == null){
                    curVideoData = new Record(fName, Integer.parseInt(currentDevice.getDeviceId()));
                    cashData.put(key, curVideoData);
                }
                
                // Сохранение и перенос данных. Если файл первый, то идем дальше.
                // Если уже сохранили тоже.
                MediaPlayer.Status status = mediaPlayer == null ? null : mediaPlayer.getStatus();
                if (status == MediaPlayer.Status.PLAYING || status == MediaPlayer.Status.PAUSED){
                    if ((prevVideoFile != null) && (!prevVideoFile.getAbsolutePath().equals(file.getAbsolutePath()))){
                        Object[] options = {"Да", "Нет"};
                        int n = JOptionPane.showOptionDialog(
                                null,
                                "Сохранить данные файла?",
                                "",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                options,
                                options[1]
                                );
                        
                        if (n == JOptionPane.YES_OPTION){
                            saveData();
                            return;
                        }
                    }
                }
                
                // Очищаем данные кэша.
                curVideoData.clearData();
                
                // Запуск видео.
                prevVideoFile = file;
                String path = file.getAbsolutePath();
                if (changeMedia(path)){
                    setEnableControlButton(true);
                }else{
                    setEnableControlButton(false);
                }
            }
        });

        jPanelListDevices.add(new JScrollPane(jListDevices), BorderLayout.NORTH);
        jPanelListDevices.revalidate();
    }
    
    //////////// common ////////////////////
    /**
     * Сохраняет данные
     */
    private void saveData(){
        // Удаляем текущий плеер.
        deletePlayer();
        
        if (!saveCurrentRecord()){
            return;
        }
        
        if (!moveVideoFile(getCurrentVideoFile())){
            return;
        }
        prevVideoFile = null;
        updateListFiles();
        startNextMedia();
    }
    
    private void startNextMedia(){
        if (listFilesModel.isEmpty()){
            setEnableControlButton(false);
            return;
        }
        
        jListFiles.setSelectedIndex(0);
        
        //Запуск видео.
        String path = getCurrentVideoFile().getAbsolutePath();
        if (changeMedia(path)){
            setEnableControlButton(true);
        }else{
            setEnableControlButton(false);
        }
    }
    
    ///////////// MultiMedia /////////////
    /**
     * Возвращает ресурс файла mp4, если он есть, иначе предварительно 
     * конвертирует его.
     * @param pathH264 путь к файлу
     * @return URI файла
     * @throws IOException 
     */
    private String getURIFileSource(String pathH264) throws IOException{
        String pathMP4 = pathH264 + MainFrame.MP4_EXTENSION;
        File mp4File = new File(pathMP4);
        if (!mp4File.exists()){
            mp4File = ConvertH264ToMP4(pathH264, pathMP4);
        }
        
        return mp4File.toURI().toString();
    }
    
    /**
     * Конверт H264 -> MP4
     * @param pathH264
     * @param pathMP4
     * @return Файл в формет MP4
     * @throws IOException 
     */
    private File ConvertH264ToMP4(String pathH264, String pathMP4) throws IOException {
        H264TrackImpl h264track = new H264TrackImpl(new DirectFileReadDataSource(new File(pathH264)));
        Movie movie = new Movie();
        movie.addTrack(h264track);
        com.coremedia.iso.boxes.Container mp4file = new DefaultMp4Builder().build(movie);
        FileOutputStream fos = new FileOutputStream(pathMP4);
        mp4file.writeContainer(fos.getChannel());
        fos.close();
        
        return new File(pathMP4);
    }
    
    private void initHotKeys() {
        // Hot keys.
        JRootPane root = this.getRootPane();
        InputMap inputMap = root.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = root.getActionMap();

        // Input++.
        KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_SHIFT, 0, true);
        inputMap.put(keyStroke, "addInput");
        actionMap.put("addInput", new AbstractAction("Add input") {
            @Override
            public void actionPerformed(ActionEvent e) {
                addInput();
            }
        });
        
        inputMap.put(KeyStroke.getKeyStroke('i'), "addInput2");
        actionMap.put("addInput2", new AbstractAction("Add input2") {
            @Override
            public void actionPerformed(ActionEvent e) {
                addInput();
            }
        });

        // Output++.
        keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_CONTROL, 0, true);
        inputMap.put(keyStroke, "addOutput");
        actionMap.put("addOutput", new AbstractAction("Ad output") {
            @Override
            public void actionPerformed(ActionEvent e) {
                addOutput();
            }
        });
        
        inputMap.put(KeyStroke.getKeyStroke('o'), "addOutput2");
        actionMap.put("addOutput2", new AbstractAction("Ad output2") {
            @Override
            public void actionPerformed(ActionEvent e) {
                addOutput();
            }
        });

        // Play, Pause, Repeat.
        inputMap.put(KeyStroke.getKeyStroke(' '), "PlayPauseRepeat");
        actionMap.put("PlayPauseRepeat", new AbstractAction("PlayPauseRepeat") {
            @Override
            public void actionPerformed(ActionEvent e) {
                playPauseRepeat();
            }
        });
        
        // SaveData.
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), "SaveData");
        actionMap.put("SaveData", new AbstractAction("SaveData") {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveData();
            }
        });
        
    }
    
    private boolean changeMedia(String path){
        try{
            displayInOut();
            
            if (WITHOUT_VIDEO){
                return true;
            }
            
            deletePlayer();
            createPlayer(path);
            return true;
        }catch(Exception ex){
            String msg = String.format("Ошибка при открытии файла. %s", 
                    ex.getMessage());
            showMessage(msg, "Ошибка");
            return false;
        }
    }
    
    private void createPlayer(String path){
        Group root = new Group();
        Scene scene = new Scene(root, 540, 241);
        
        // Получаем ресурс файла.
        String src;
        try {
            src = getURIFileSource(path);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            String msg = String.format("Ошибка, при конвертировании файла [%s]",
                    ex.getMessage());
            showMessage(msg, "Ошибка");
            return;
        }

        curMedia = new Media(src);
        
        // Создаем плеер.
        mediaPlayer = new MediaPlayer(curMedia);
        mediaPlayer.setAutoPlay(true);
        mediaControl = new MediaControl(mediaPlayer);
        scene.setRoot(mediaControl);
        
        // Переопределяем событие на repeat, чтобы добавить обнуление данных.
        Button repeatButton = mediaControl.getRepeatButton();
        repeatButton.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                repeatPlayer();
            }
        });
    
        
         // Размер player.
        int h = fxPanel.getHeight();
        int w = fxPanel.getWidth();
        mediaControl.getMediaView().setFitHeight(h - 50);
        mediaControl.getMediaView().setFitWidth(w);
        mediaControl.setCenter(mediaControl.getMediaView());
        
        // DropShadow effect
        DropShadow dropshadow = new DropShadow();
        dropshadow.setOffsetY(5.0);
        dropshadow.setOffsetX(5.0);
        dropshadow.setColor(Color.WHITE);
        mediaControl.getMediaView().setEffect(dropshadow);

        fxPanel.setScene(scene);
    }
    
    private void deletePlayer(){
        if (mediaPlayer != null){
            mediaPlayer.stop();
        }
        fxPanel.removeAll();
    }
    
    private void playPlayer(){
        // Если тестовый режим, без видео.
        if (this.WITHOUT_VIDEO){
            return;
        }
        
        if (mediaControl == null){
            this.showMessage("Видеоплеер не создан.", "");
            return;
        }
        
        if (curVideoData == null){
            this.showMessage("Выберите видео файл.", "");
            return;
        }
        
        mediaControl.play();
    }
    
    private void pausePlayer(){
        // Если тестовый режим, без видео.
        if (this.WITHOUT_VIDEO){
            return;
        }
        
        if (mediaControl == null){
            this.showMessage("Видеоплеер не создан.", "");
            return;
        }
        
        if (curVideoData == null){
            this.showMessage("Выберите видео файл.", "");
            return;
        }
        
        mediaControl.pause();
    }
    
    /**
     * Повтор.
     */
    private void repeatPlayer(){
        // Обнуляем вход и выход.
        this.clearInputOutputDataAndTextField();
        
        // Если тестовый режим, без видео.
        if (this.WITHOUT_VIDEO){
            return;
        }
        
        if (mediaControl == null){
            this.showMessage("Видеоплеер не создан.", "");
            return;
        }
        
        if (curVideoData == null){
            this.showMessage("Выберите видео файл.", "");
            return;
        }
        
        mediaControl.repeat();
    }
    
    private void playPauseRepeat(){
         MediaPlayer.Status status = mediaPlayer == null ? null : mediaPlayer.getStatus();
         if (status == null){
             this.showMessage("Выберите видео файл.", "");
             return;
         }
         
         if (status == MediaPlayer.Status.PAUSED){
             playPlayer();
             return;
         }
         
         if (mediaControl.getAtEndOfMedia()){
             repeatPlayer();
             return;
         }
         
         if (status == MediaPlayer.Status.PLAYING){
             pausePlayer();
         }
    }

    ///////////// UI /////////////
    private void addInput(){
        if (curVideoData != null){
            curVideoData.addInput();
            jTextFieldIn.setText(String.valueOf(curVideoData.getInput()));
        }else{
            this.showMessage("Выберите видео файл.", "");
        }
    }
    
    private void addOutput(){
        if (curVideoData != null){
            curVideoData.addOutput();
            jTextFieldOut.setText(String.valueOf(curVideoData.getOutput()));
        }else{
            this.showMessage("Выберите видео файл.", "");
        }
    }
    
    
    /**
     * Обновляет список файлов.
     */
    private void updateListFiles(){
        // Меняем список файлов.
        listFilesModel.removeAllElements();
        int i = 0;
        for (File file : currentDevice.getDir().listFiles(geth264FileFilter())) {
            listFilesModel.add(i, file);
            i++;
        }

        // Сортируем файлы.
        sortModel(listFilesModel);
    }
    
    private void sortModel(DefaultListModel model) {
        List list = new ArrayList<>();
        for (int i = 0; i < model.size(); i++) {
            list.add(model.get(i));
        }
        
        Collections.sort(list);
        
        model.removeAllElements();
        for (Object s : list) {
            model.addElement(s);
        }
    }
    
    /**
     * Показывает сообщения пользователю.
     * @param msg 
     */
    private void showMessage(String msg, String title){
        JOptionPane.showConfirmDialog(null, msg, title, 
                    JOptionPane.CLOSED_OPTION);
    }
    
    /**
     * Делаем группу кнопок доступными или недоступными.
     * @param mode 
     */
    private void setEnableControlButton(boolean mode){
        jButtonInPlus.setEnabled(mode);
        jButtonOutPlus.setEnabled(mode);
        jButtonSendData.setEnabled(mode);
    }
    
    private void displayInOut(){
        int in = curVideoData.getInput();
        int out = curVideoData.getOutput();
        
        jTextFieldIn.setText(String.valueOf(in));
        jTextFieldOut.setText(String.valueOf(out));
    }
    
    /**
     * Обнуляем значения входа и выхода.
     */
    private void clearInputOutputDataAndTextField(){
        if (curVideoData != null){
            curVideoData.clearData();
        }
        
        String value = "0";
        jTextFieldIn.setText(value);
        jTextFieldOut.setText(value);
    }
    
    private void clearCalculateTextField(){
        jTextFieldAverage.setText("");
        jTextFieldBalance.setText("");
    }
    
    ////////// Работа с файловой системой ////////////////
    /**
     * Возвращает фильтр файлов с расширением .h264
     * @return 
     */
    private FileFilter geth264FileFilter(){
        FileFilter h264FileFilter = new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.isDirectory()) {
                    return false;
                }
                
                String fname = file.getName();
                if (fname.endsWith(MainFrame.H264_EXTENSION)){
                    return true;
                }
                
                return false;
            }
        };
        return h264FileFilter;
    }
    
    private File getCurrentVideoFile(){
        File file = (File) jListFiles.getSelectedValue();
        return file;
    }
    
    /**
     * Перенос файла, в директорию "дата из имени файла".
     * @return 
     */
    private boolean moveVideoFile(File file){
        // Создаем директорию.
        String dirName = file.getName().split("[_]")[0];
        
        File destDir = new File(String.format("%s/%s", currentDevice.getDir().getAbsolutePath(), dirName));
        if (!destDir.exists()){
            boolean created = destDir.mkdir();
            if (!created){
                showMessage("Ошибка, при создании подкаталога: " + destDir.getAbsolutePath() , "Error");
                return false;
            }
        }

        // Переносим туда файл h264
        boolean rename = file.renameTo(new File(destDir, file.getName()));
        if (!rename) {
            showMessage("Ошибка, при переносе файла: " + file.getAbsolutePath(), "Error");
            return false;
        }

        // Удаляем файл Mp4, если есть.
        File mp4File = new File(String.format("%s/%s.mp4", currentDevice.getDir().getAbsolutePath(), 
                file.getName()));
        if (mp4File.exists()){
            boolean delete = mp4File.delete();
            if (!delete){
                showMessage("Ошибка, при удалении файла: " + mp4File.getAbsolutePath(), "Error");
                return false;
            }
        }
        return true;
    }
    
    ///////////Запросы к БД/////////////
    private boolean saveCurrentRecord() {
        // Создаем таблицу, если ее не существует.
        String sql = String.format("CREATE TABLE if not exists %s "
                + "(tb_man_count_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + " file_name varchar(64), "
                + " unit_id INTEGER, "
                + " in_pass INTEGER, "
                + " out_pass INTEGER, "
                + " operator int,"
                + " d_time DATETIME,"
                + " count_type,"
                + " count_proability real, "
                + "UNIQUE (file_name, unit_id));", tbManCount);
        boolean res = db.executeUpdate(sql);
        if (!res){
            JOptionPane.showMessageDialog(null, 
                    "Не удалось создать таблицу.", 
                    "", JOptionPane.CLOSED_OPTION);
            return false;
        }
        
        
        // Текущая дата в формате.
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String now = sdf.format(date);

        // Обновляем или создаем запись.
        sql = String.format("INSERT OR IGNORE INTO %1$s (file_name, unit_id) "
                + "VALUES('%2$s', %5$s);"
                + "UPDATE %1$s SET "
                + "unit_id = %5$s, "
                + "d_time= '%6$s', "
                + "count_type= 0, "
                + "in_pass = %3$s, "
                + "out_pass = %4$s "
                + "where file_name = '%2$s' and unit_id=%5$s",
                tbManCount, curVideoData.getName(), curVideoData.getInput(),
                curVideoData.getOutput(), currentDevice.getDeviceId(), now);
        
        res = db.executeUpdate(sql);
        if (!res){
            JOptionPane.showMessageDialog(null, 
                    "Не удалось сделать запись в БД.", 
                    "", JOptionPane.CLOSED_OPTION);
            return false;
        }
        
        // Считаем дисбаланс и среднее.
        res = this.calculate();
        if (!res){
            JOptionPane.showMessageDialog(null, 
                    "Не удалось рассчитать дисбаланс.", 
                    "", JOptionPane.CLOSED_OPTION);
            return false;
        }
        
        JOptionPane.showMessageDialog(null, 
                    "Данные отправлены.", 
                    "", JOptionPane.CLOSED_OPTION);
        return true;
    }
    
    /**
     * Возвращает запись из БД, если есть, иначе null.
     * @param fileName 
     * @return 
     */
    private Record getRecord(String fileName){
        String sql = String.format("select * from %s where file_name='%s' and "
                + "unit_id=%s", 
                tbManCount, fileName, currentDevice.getDeviceId());
        
        try {
            ResultSet rs = db.executeQuery(sql);
            if (rs == null){
                return null;
            }
            
            while (rs.next()){
                String fName = rs.getString("file_name");
                int in_pass = rs.getInt("in_pass");
                int out_pass = rs.getInt("out_pass");
                int unit_id = rs.getInt("unit_id");
                
                Record rec = new Record(fileName, unit_id, in_pass, out_pass);
                return rec;
            }
            
        } catch (NullPointerException | SQLException ex1) {
            return null;
        } 
        
        return null;
    }
    
    private String getTsNumber(String deviceName) {
        String sql = String.format("SELECT ts.ts_gos_num FROM %s as ts\n" +
"join %s as ts_link on ts.ts_id=ts_link.ts_id\n" +
"where ts_link.unit_id=%s\n" +
"limit 1", tbTransport, tbTsUnitLink, deviceName, deviceName);
        
        ResultSet rs;
        try {
            rs = db.executeQuery(sql);
            while (rs.next()){
                return rs.getString("ts_gos_num");
            }
        } catch (NullPointerException | SQLException ex1) {
            return null;
        } 
        
        return null;
        
    }
    
    private boolean calculate(){
        boolean res = calculateDisbalance();
        if (!res){
            return res;
        }
        
        return calculateAverage();
    }
    
    private boolean calculateAverage(){
        String sql = String.format(
                "select (sum(in_pass + out_pass) / 2.0) as 'average' from %1$s "
                + "where unit_id in (select unit_id from %2$s "
                + "where ts_id = (select ts_id from %2$s "
                + "where unit_id=%3$s));", this.tbManCount, this.tbTsUnitLink, 
                currentDevice.getDeviceId());
        
        if (this.DEBUG){
            System.out.println(sql);
        }
        
        ResultSet rs = db.executeQuery(sql);
        if (rs != null){
            try {
                while (rs.next()){
                    double average = rs.getFloat("average");
                    jTextFieldAverage.setText(String.format("%.2f", average));
                    
                    if (this.DEBUG){
                        System.out.println(String.format("Balance: %.2f", average));
                    }

                    return true;
                }
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        return false;
    }
    
    private boolean calculateDisbalance(){
        String sql = String.format(
                "select (sum(in_pass) - sum(out_pass)) / (sum(in_pass + out_pass) / 2.0) as 'balance' from %1$s "
                + "where unit_id in (select unit_id from %2$s "
                + "where ts_id = (select ts_id from %2$s "
                + "where unit_id=%3$s));", this.tbManCount, this.tbTsUnitLink, 
                currentDevice.getDeviceId());
        
        if (this.DEBUG){
            System.out.println(sql);
        }
        
        ResultSet rs = db.executeQuery(sql);
        if (rs != null){
            try {
                while (rs.next()){
                    double balance = rs.getFloat("balance");
                    jTextFieldBalance.setText(String.format("%.2f", balance));
                    
                    if (this.DEBUG){
                        System.out.println(String.format("Balance: %.2f", balance));
                    }

                    return true;
                }
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


        return false;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jEastPanel = new javax.swing.JPanel();
        jPanelListDevices = new javax.swing.JPanel();
        jPanelFiles = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jWestPanel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanelVideo = new javax.swing.JPanel();
        jPanelVideoBar = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldIn = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldOut = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jTextFieldBalance = new javax.swing.JTextField();
        jTextFieldAverage = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jButtonInPlus = new javax.swing.JButton();
        jButtonOutPlus = new javax.swing.JButton();
        jButtonSendData = new javax.swing.JButton();
        jMenuBar = new javax.swing.JMenuBar();
        jMenuHelp = new javax.swing.JMenu();
        jMenuItemHotKeys = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jEastPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jPanelListDevices.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanelListDevices.setLayout(new java.awt.BorderLayout());

        jPanelFiles.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanelFiles.setLayout(new java.awt.BorderLayout());

        jLabel1.setText("Список устройств");

        jLabel2.setText("Список файлов");

        javax.swing.GroupLayout jEastPanelLayout = new javax.swing.GroupLayout(jEastPanel);
        jEastPanel.setLayout(jEastPanelLayout);
        jEastPanelLayout.setHorizontalGroup(
            jEastPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jEastPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jEastPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelListDevices, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jPanelFiles, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jEastPanelLayout.setVerticalGroup(
            jEastPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jEastPanelLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelListDevices, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelFiles, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
                .addGap(32, 32, 32))
        );

        jWestPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jWestPanel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jWestPanelKeyPressed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel8.setText("Video");

        jPanelVideo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanelVideo.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addContainerGap(582, Short.MAX_VALUE))
            .addComponent(jPanelVideo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelVideo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel3.setText("По файлу:");

        jLabel4.setText("вход");

        jTextFieldIn.setEditable(false);

        jLabel5.setText("выход");

        jTextFieldOut.setEditable(false);

        jLabel6.setText("По ТС за сутки:");

        jLabel7.setText("дисбаланс (%)");

        jTextFieldBalance.setEditable(false);

        jTextFieldAverage.setEditable(false);

        jLabel9.setText("среднее");

        jButtonInPlus.setText("In++");
        jButtonInPlus.setEnabled(false);
        jButtonInPlus.setFocusable(false);
        jButtonInPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInPlusActionPerformed(evt);
            }
        });

        jButtonOutPlus.setText("out++");
        jButtonOutPlus.setEnabled(false);
        jButtonOutPlus.setFocusable(false);
        jButtonOutPlus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOutPlusActionPerformed(evt);
            }
        });

        jButtonSendData.setText("Внести подсчет");
        jButtonSendData.setEnabled(false);
        jButtonSendData.setFocusable(false);
        jButtonSendData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSendDataActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelVideoBarLayout = new javax.swing.GroupLayout(jPanelVideoBar);
        jPanelVideoBar.setLayout(jPanelVideoBarLayout);
        jPanelVideoBarLayout.setHorizontalGroup(
            jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelVideoBarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonInPlus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonOutPlus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelVideoBarLayout.createSequentialGroup()
                        .addGroup(jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(jPanelVideoBarLayout.createSequentialGroup()
                                .addGroup(jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextFieldOut, javax.swing.GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE)
                                    .addComponent(jTextFieldIn)))
                            .addComponent(jLabel6)
                            .addGroup(jPanelVideoBarLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(9, 9, 9)
                                .addComponent(jTextFieldBalance, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelVideoBarLayout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(49, 49, 49)
                                .addComponent(jTextFieldAverage, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jButtonSendData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanelVideoBarLayout.setVerticalGroup(
            jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelVideoBarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldIn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextFieldOut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jTextFieldAverage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelVideoBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jTextFieldBalance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonInPlus)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButtonOutPlus)
                .addGap(45, 45, 45)
                .addComponent(jButtonSendData, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jWestPanelLayout = new javax.swing.GroupLayout(jWestPanel);
        jWestPanel.setLayout(jWestPanelLayout);
        jWestPanelLayout.setHorizontalGroup(
            jWestPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jWestPanelLayout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(jPanelVideoBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jWestPanelLayout.setVerticalGroup(
            jWestPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanelVideoBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jMenuHelp.setText("Справка");

        jMenuItemHotKeys.setText("Горячие клавиши");
        jMenuItemHotKeys.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemHotKeysActionPerformed(evt);
            }
        });
        jMenuHelp.add(jMenuItemHotKeys);

        jMenuBar.add(Box.createHorizontalGlue());

        jMenuBar.add(jMenuHelp);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jEastPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jWestPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jWestPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jEastPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSendDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSendDataActionPerformed
        saveData();
    }//GEN-LAST:event_jButtonSendDataActionPerformed

    private void jButtonInPlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonInPlusActionPerformed
        addInput();
    }//GEN-LAST:event_jButtonInPlusActionPerformed

    private void jButtonOutPlusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOutPlusActionPerformed
        // TODO add your handling code here:
        addOutput();
    }//GEN-LAST:event_jButtonOutPlusActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        db.close();
    }//GEN-LAST:event_formWindowClosing

    private void jWestPanelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jWestPanelKeyPressed
        // TODO add your handling code here:
        int keyCode = evt.getKeyCode();
        System.out.println(keyCode);
    }//GEN-LAST:event_jWestPanelKeyPressed

    private void jMenuItemHotKeysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemHotKeysActionPerformed
        // TODO add your handling code here:
        final JEditorPane pane = new JEditorPane();
        pane.setContentType("text/html");
        pane.setEditable(false);
        pane.setText("<html>"
+ "<h2>Горячие клавиши:</h2>"
    + "<ul>"
        + "<li><b>shift</b> или <b>i</b> или <i>кнопка Input++</i> - увеличить кол-во входящих на один"
        + "</li>"
        + "<li><b>ctrl</b> или <b>o</b> или <i> кнопка Output++ </i> - увеличить кол-во выходящих на один </li>"
        + "<li><b>пробел</b> - старт, пауза </li>"
        + "<li><b>enter</b> - отправить данные </li>"
    + "</ul>"
+ "</html>"
        );
        
        // stuff it in a scrollpane with a controlled size.
        JScrollPane scrollPane = new JScrollPane(pane);		
        scrollPane.setSize(new Dimension(600, 500));

        // pass the scrollpane to the joptionpane.				
        JOptionPane.showMessageDialog(null, scrollPane, "Hotkeys", 
                JOptionPane.INFORMATION_MESSAGE);

    }//GEN-LAST:event_jMenuItemHotKeysActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                String dbPath = null;
                String dirPath = null;
                if (args.length == 2) {
                    dbPath = args[0];
                    dirPath = args[1];
                    
                    System.out.println(dbPath);
                    System.out.println(dirPath);
                }
                
                MainFrame frame = new MainFrame(dbPath, dirPath);
                frame.setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButtonInPlus;
    private javax.swing.JButton jButtonOutPlus;
    private javax.swing.JButton jButtonSendData;
    private javax.swing.JPanel jEastPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuHelp;
    private javax.swing.JMenuItem jMenuItemHotKeys;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanelFiles;
    private javax.swing.JPanel jPanelListDevices;
    private javax.swing.JPanel jPanelVideo;
    private javax.swing.JPanel jPanelVideoBar;
    private javax.swing.JTextField jTextFieldAverage;
    private javax.swing.JTextField jTextFieldBalance;
    private javax.swing.JTextField jTextFieldIn;
    private javax.swing.JTextField jTextFieldOut;
    private javax.swing.JPanel jWestPanel;
    // End of variables declaration//GEN-END:variables

}
