/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author virgo
 */
public class SqliteDB {
    // Если true, то выводит трэйс ошибок.
    private boolean DEBUG = false;

    private String dbPath = "./data/data.db";
    private String driverName = "org.sqlite.JDBC";
    private Connection conn;
    private Statement stmt;

    public SqliteDB(String dbPath) {
        this.dbPath = dbPath == null ? this.dbPath : dbPath;
        conn = getConnection();
    }
    
    public String getDbPath(){
        return this.dbPath;
    }

    public boolean close() {
        try {
            conn.close();
            System.out.println("Close connection.");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SqliteDB.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Ручной коммит. Использовать, если автокоммит == false.
     * @return 
     */
    public boolean commit() {
        try {
            this.getConnection().commit();
            return true;
        } catch (SQLException ex) {
            if (DEBUG)
                Logger.getLogger(SqliteDB.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private Connection getConnection() {
        try {
            if (conn != null) {
                if (!conn.isClosed()){
                    return conn;
                }
            }

            Class.forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.println("Opened database successfully");
            return conn;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SqliteDB.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(SqliteDB.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Устанавливает автокоммит для БД.
     * @param bool 
     * @return 
     */
    public boolean setAutoCommit(Boolean bool) {
        try {
            conn.setAutoCommit(bool);
            return true;
        } catch (SQLException ex) {
            if (DEBUG)
                Logger.getLogger(SqliteDB.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Выполняет запрос (select), который должне возвратить данные.
     * Не забыть вызвать после обработки данных. 
     * closeAfterExecuteQuery()
     * @param sql
     * @return набор данных ResultSet
     */
    public ResultSet executeQuery(String sql) {
        try {
            stmt = this.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            return rs;
        } catch (SQLException ex) {
            return null;
        }
    }
    
    /**
     * Закрывает соединения после executeQuery().
     * @param rs
     * @return 
     */
    public boolean closeAfterExecuteQuery(ResultSet rs){
        try {
            stmt.close();
            rs.close();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    /**
     * Запросы, типа update, insert, create.
     * @param sql
     * @return 
     */
    public boolean executeUpdate(String sql) {
        try {
            Statement stmt = this.getConnection().createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            return true;
        } catch (SQLException ex) {
            if (DEBUG)
                Logger.getLogger(SqliteDB.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static void unitTest(String args[]) {
        System.out.println("Sqlite");
        SqliteDB sqlite = new SqliteDB("./data/test.db");
        try {
            sqlite.test();
        } catch (SQLException ex) {
            Logger.getLogger(SqliteDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Метод для проверки и примеров работы класса.
     * @throws SQLException 
     */
    public void test() throws SQLException {
        // insert
        String sql = "CREATE TABLE COMPANY "
                + "(ID INT PRIMARY KEY     NOT NULL,"
                + " NAME           TEXT    NOT NULL, "
                + " AGE            INT     NOT NULL, "
                + " ADDRESS        CHAR(50), "
                + " SALARY         REAL);";
        this.executeUpdate(sql);
        
        // insert
        sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
                + "VALUES (1, 'Allen', 25, 'Texas', 15000.00 );";
        this.executeUpdate(sql);

        // insert, autocommit == false
        this.setAutoCommit(false);
        sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
                + "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );";
        this.executeUpdate(sql);
        sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
                + "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );";
        this.executeUpdate(sql);
        sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
                + "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";
        this.executeUpdate(sql);
        this.commit();
        this.setAutoCommit(true);

        // update
        sql = "UPDATE COMPANY set SALARY = 25000.00 where ID=1;";
        this.executeUpdate(sql);

        // delete
        sql = "DELETE from COMPANY where ID=4;";
        this.executeUpdate(sql);
        
        // select
        sql = "SELECT * FROM COMPANY;";
        ResultSet rs = this.executeQuery(sql);
        while (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("name");
            int age = rs.getInt("age");
            String address = rs.getString("address");
            float salary = rs.getFloat("salary");
            System.out.println("ID = " + id);
            System.out.println("NAME = " + name);
            System.out.println("AGE = " + age);
            System.out.println("ADDRESS = " + address);
            System.out.println("SALARY = " + salary);
            System.out.println();
        }
        this.closeAfterExecuteQuery(rs);
        
        // drop
        sql = "drop table COMPANY";
        boolean res = this.executeUpdate(sql);
        if (res == false){
            System.out.println("Not DROP");
        }

        // close
        this.close();
    }

}
