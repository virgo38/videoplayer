/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Date;

/**
 *
 * @author virgo
 */
public class Record {
    private int tbManCountId;   // pk
    private int unitId;     // id устройства
    private String fileName;
    private int inPass = 0;
    private int outPass = 0;
    private int operatorId;
    private Date dTime;     // время изменения записи.
    private int countType = 0;
    private float countProability;

    public Record(String fileName, int unitId) {
        this.fileName = fileName;
        this.unitId = unitId;
    }

    public Record(String fileName, int unitId, int in_pass, int out_pass) {
        this.fileName = fileName;
        this.inPass = in_pass;
        this.outPass = out_pass;
    }
    
    public int getUnitId(){
        return this.unitId;
    }
    
    public int addInput(){
        return ++this.inPass;
    }
    
    public int subIn(){
        this.inPass = this.inPass == 0 ? 0 : this.inPass - 1;
        return inPass;
    }

    public int addOutput(){
        return ++this.outPass;
    }
    
    public int subOutput(){
        this.outPass = this.outPass == 0 ? 0 : this.outPass - 1;
        return outPass;
    }
    
    public String getName(){
        return fileName;
    }

    public int getInput() {
        return inPass;
    }

    public int getOutput() {
        return outPass;
    }

    /**
     * Обнуляет кол-во вошедших и вышедших пассажиров.
     */
    public void clearData() {
        inPass = 0;
        outPass = 0;
    }
    
    
}
